// binary radix_final.cpp : Defines the entry point for the console application.
//
//komentarrr
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>

using namespace std;

bool Branje_Stevil(vector<int> &vec, const char s[]) {
	ifstream input(s);
	long long unsigned st;

	if (!input.is_open()) {
		return false;
	}

	while (!input.eof()) {
		input >> st;
		vec.push_back(st);
		while (isspace(input.peek())) input.get();
	}
	input.close();
	return true;
}

void Izpis_Stevil(vector<int>  &polje) {
	ofstream output("out.txt");

	for (int i = 0; i<polje.size(); i++)
		output << polje[i] << ' ';
}

void Izpis_Stevil_Konzola(vector<int>  &polje) {

	for (int i = 0; i < polje.size(); i++)
	{
		cout << polje[i] << ' ';
	}
	cout << endl;
}

void intToBit(vector <int> &beri, vector<int> &b, vector<string> &arrStr)
{
	string bit;
	stringstream ss;
	string push;
	stringstream ssbit;
	for (int i = 0; i<beri.size(); i++)
	{
		int a = beri[i];

		for (int j = 0; j<8; j++)
		{
			if (a >= b[j])
			{
				ssbit << 1;
				a = a - b[j];
			}
			else
			{
				ssbit << 0;
			}
		}
		bit = ssbit.str();
		arrStr.push_back(bit);
		bit = "";
		ssbit.str("");
	}

}

void sortirajBinary(vector<string> &arrStr, vector<string> &sortedBinary)
{
	vector<string> a = arrStr;
	vector<string> b;

	string pretvori;

	char c[9];

	for (int i = 7; i >= 0; i--)
	{
		for (int j = 0; j < a.size(); j++)
		{
			strcpy_s(c, a[j].c_str());
			if (c[i] == '0')
			{
				b.push_back(a[j]);
			}
		}

		for (int k = 0; k < a.size(); k++)
		{
			strcpy_s(c, a[k].c_str());
			if (c[i] == '1')
			{
				b.push_back(a[k]);
			}
		}

		a = b;
		b.clear();
	}

	sortedBinary = a;
}

void bitToInt(vector<string> &sortedBinary, vector<int> &zakljucek, vector<int> &b)
{
	char c[9];
	int push = 0;

	for (int i = 0; i<sortedBinary.size(); i++)
	{
		strcpy_s(c, sortedBinary[i].c_str());
		for (int j = 0; j < 8; j++)
		{
			if (c[j] == '1')
			{
				push = push + b[j];
			}
		}
		zakljucek.push_back(push);
		push = 0;
	}
}

int main(int argc, const char* argv[]) {
	
	vector <int> beri;
	

	if (argc < 2) return 0;
	if (!Branje_Stevil(beri, argv[1]))
	{
		return 0;
	}

	vector<int> b;
	b.push_back(128);
	b.push_back(64);
	b.push_back(32);
	b.push_back(16);
	b.push_back(8);
	b.push_back(4);
	b.push_back(2);
	b.push_back(1);

	vector<int> zakljucek;

	string bit;
	vector<string> strArr;
	vector<string> sortedBinary;

	int velikost = beri.size();
	intToBit(beri, b, strArr);
	sortirajBinary(strArr, sortedBinary);
	bitToInt(sortedBinary, zakljucek, b);
	
	Izpis_Stevil(zakljucek);
	
	return 0;
}


