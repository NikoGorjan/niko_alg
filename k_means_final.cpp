// k_means.cpp : Defines the entry point for the console application.
//
//komentar
#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <sstream>

using namespace std;

struct Tocka
{
	double x;
	double y;
	Tocka *pripadnost;
};

bool BeriTocke(vector<Tocka*> &ret,const char s[])
{
	//vector<Tocka*> ret;
	ifstream myfile(s);

	if (!myfile.is_open()) {
		return false;
	}

	double a;
	double b;

	while (myfile >> a >> b)
	{
		Tocka * push = new Tocka();
		push->x = a;
		push->y = b;
		ret.push_back(push);
	}
	myfile.close();
	return true;
	//return ret;
}

void K_means(vector<Tocka*> &vec, const char ii[], const char gg[]) //argv[1]
{
	srand(time(0));
	int xx;
	int yy;
	stringstream ss;
	stringstream zz;

	ss << ii;
	zz << gg;

	unsigned int iteracije;
	unsigned int gruce;

	ss >> iteracije;
	zz >> gruce;

	int k = 5;//iteracije*gruce;

	vector<Tocka*> gruc;
	for (int i = 0; i<gruce; i++)
	{
		xx = rand() % k;
		yy = rand() % k;
		Tocka * gr = new Tocka();
		gr->x = xx;
		gr->y = yy;
		gruc.push_back(gr);
	}


	vector<int> oddaljenost;
	double vrednost;
	double min = 99999;

	for (int j = 0; j<iteracije; j++)
	{

		for (int k = 0; k<vec.size(); k++)
		{

			for (int l = 0; l<gruc.size(); l++)
			{
				vrednost = pow(gruc[l]->x - vec[k]->x, 2) + pow(gruc[l]->y - vec[k]->y, 2);
				oddaljenost.push_back(vrednost);
			}

			for (int m = 0; m<oddaljenost.size(); m++)
			{
				if (oddaljenost[m]<min)
				{
					min = oddaljenost[m];
				}
			}

			for (int n = 0; n<oddaljenost.size(); n++)
			{
				if (oddaljenost[n] == min)
				{
					vec[k]->pripadnost = gruc[n];
				}
			}
			oddaljenost.clear();
			min = 99999;

		}
		//Spremeni vrednosti gruc
		double novaVrednostX = 0;
		double novaVrednostY = 0;

		int stevec = 0;

		for (int o = 0; o<gruc.size(); o++)
		{
			for (int p = 0; p<vec.size(); p++)
			{
				if (gruc[o] == vec[p]->pripadnost)
				{
					novaVrednostX = novaVrednostX + vec[p]->x;
					novaVrednostY = novaVrednostY + vec[p]->y;
					stevec = stevec + 1;
				}
			}

			if (stevec != 0)
			{
				gruc[o]->x = novaVrednostX / stevec;
				gruc[o]->y = novaVrednostY / stevec;

				novaVrednostX = 0;
				novaVrednostY = 0;
				stevec = 0;
			}
		}


	}

	ofstream output("out.txt");

	for (int r = 0; r<vec.size(); r++)
	{
		output << r << " ";
		for (int s = 0; s<gruc.size(); s++)
		{
			if (gruc[s] == vec[r]->pripadnost)
			{
				output << s << endl;
			}
		}
	}


}


int main(int argc, const char* argv[])
{
	
	vector<Tocka*> vec1;
	if (argc < 4) return 0;
	if (!BeriTocke(vec1,argv[3])) return 0;
	

	K_means(vec1, argv[1], argv[2]);


	system("pause");
	return 0;
}

